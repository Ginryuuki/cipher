#include "cipher.hpp"
#include "util.hpp"

#include <memory>
#include <fstream>
#include <iostream>
#include <cstdlib>

namespace cipher {
    caesar::caesar(std::unique_ptr<args> args)
    {
        /**
         * Initialize variables
         */
        this->content           = args->get_content();
        this->processed_content = "";
        this->key               = args->get_key();
        this->str_map           = "abcdefghijklmnopqrstuvwxyz"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "1234567890"
            "!@#$%^&*()-=_+[]{}:\"|;'\\,./<>?";

        for (int i = 0 ; i < int(this->str_map.size()) ; i++)
            this->char_map[str_map[i]] = i;

        if (args->is_decrypt())
            this->key *= -1;

        /**
         * Begin cipher
         */

        for (char ch : this->content) {
            auto search = this->char_map.find(ch);
            if (search == this->char_map.end()) {
                /** Exceptions are whitespaces */
                if (!(ch == '\n' || ch == 10 || ch == 9 || ch == 32)) {
                    std::cerr << "Error: Unsupported characted detected\n";
                    util::exit_fail();
                }
                this->processed_content.push_back(ch);
                continue;
            }
            int index = search->second + this->key;

            if (index < 0)
                index += str_map.size();

            if (index >= int(str_map.size()))
                index -= str_map.size();

            this->processed_content.push_back(this->str_map[index]);
        }

        /**
         * Outputs
         */
        if (!args->get_output_file().empty()) {
            std::ofstream output_stream(args->get_output_file().data());
            output_stream << this->processed_content;
            output_stream.close();
#ifdef __DEBUG__
            std::cout << "Debug: Output\n" << this->processed_content;
#endif
            return;
        }

        std::cout << this->processed_content << '\n';
    }
}
