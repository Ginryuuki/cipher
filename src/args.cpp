#include "cipher.hpp"
#include "util.hpp"

#include <string_view>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <regex>
#include <cstdlib>
#include <ios>

#include <getopt.h>

/**
 * Misc functions for args
 */

void version()
{
    std::cout << "cipher-cli version 1.0\n"
        "   This is program is under BSD-2 Clause License\n"
        "   This is program is not subject to any form of warranty\n";
    std::exit(EXIT_SUCCESS);
}

void help()
{
    std::cout << "cipher-cli - A simple cipher program\n";
    std::cout << "Usage: cipher-cli [OPTION...] INPUT\n";
    std::cout << "Options: \n"
        "   --help    -h       Print this message\n"
        "   --version -v       Show program version\n"
        "   --key     -k       KEY value for cipher\n"
        "   --input   -i       FILE input target, should only have one target\n"
        "   --output  -o       FILE output target\n";
    std::exit(EXIT_SUCCESS);
}

/**
 * This is where cipher::args methods live
 */

namespace cipher {

    /**
     * Public methods
     */

    args::args(int argc, char **argv)
    {
        this->content     = "";
        this->output_file = "";
        this->decrypt     = false;
        /** We set this to -1 to catch errors easily */
        this->key         = -1;

        /**
         * We process args here
         */
        option cipher_opts[] = {
            { "key"    , required_argument, 0, 'k' },
            { "input"  , required_argument, 0, 'i' },
            { "output" , required_argument, 0, 'o' },
            { "decrypt", no_argument,       0, 'd' },
            { "version", no_argument,       0, 'v' },
            { "help",    no_argument,       0, 'h' },
            { 0,        0,                  0,  0  } 
        };

        int c, option_index;

        while (true) {
            option_index = 0;
            c = getopt_long(argc, argv, "k:i:o:dvh", cipher_opts, &option_index);

            if (c == -1)
                break;

#ifdef __DEBUG__
            std::cout << "Debug: optarg = " << optarg << '\n';
#endif

            switch (c) {
                case 'v':
                    version();
                    break;
                case 'h':
                    help();
                    break;
                case 'k':
                    this->set_key(optarg);
                    break;
                case 'i':
                    this->set_content_from_file(optarg);
                    break;
                case 'o':
                    this->set_output_file(optarg);
                    break;
                case 'd':
                    this->turn_on_decrypt();
                    break;
#ifdef __DEBUG__
                default:
                    std::cout << "Debug: Getopt returned code" << std::oct << c << '\n';
#endif
            }
        }

#ifdef __DEBUG__
        std::cout << "Debug: argc = " << argc << ", optind = " << optind << '\n';
#endif

        if (optind < argc && (optind - argc) != 1) {
            std::string val = argv[optind++];
#ifdef __DEBUG__
            std::cout << "Debug: argv[optind++] = " << val << '\n';
#endif
            this->set_content(val);
        }
        else if (optind != argc) {
            std::cerr << "Error: Dangling arguments detected\n";
            util::exit_fail();
        }

        /**
         * Sanity checks
         */
        auto sanity_die = [](std::string_view message) {
            std::cerr << "Error: " << message << '\n';
            util::exit_fail();
        };

        if (this->key == -1)
            sanity_die("No KEY value given");

        if (this->content.empty())
            sanity_die("No target INPUT given");

#ifdef __DEBUG__
        std::cout << "Debug: content\n" << this->content;
#endif

    }

    /**
     * This is where we handle getting the string to be processed
     */
    void args::set_content(std::string_view content)
    {
        /** Catch if non-empty content */
        this->is_content_empty();
#ifdef __DEBUG__
        std::cout << "Debug: Assigning content to: " << content << '\n';
#endif
        this->content.append(content);
    }

    /**
     * This is where we handle out input file
     */
    void args::set_content_from_file(std::string_view content)
    {
        /** Should not be called twice */
        this->is_content_empty();

        if (!std::filesystem::is_regular_file(content.data())) {
            std::cerr << "Error: INPUT file is not a regular file\n";
            util::exit_fail();
        }

        std::ifstream input_file(content.data());
        std::string line;

        if (input_file.is_open()) {
            while (std::getline(input_file, line)) {
                line.push_back('\n');
                this->content.append(line);
            }
            input_file.close();
        }
        else {
            std::cerr << "Error: Unable to open INPUT file\n";
            util::exit_fail();
        }

    }

    /**
     * Decrypt mode on. We don't care how many times this will be called
     */
    void args::turn_on_decrypt()
    {
        this->decrypt = true;
    }

    /**
     * Cipher key
     */
    void args::set_key(std::string_view input_key)
    {
        /** Catch duplicate call */
        if (this->key == 1) {
            std::cerr << "Error: Target KEY already exists\n";
            util::exit_fail();
        }

        auto die_key = []() {
            std::cerr << "Error: Invalid numeric value on KEY\n";
            util::exit_fail();
        };

        /** Catch non-numerics early */
        if (!std::regex_match(input_key.data(), std::regex("\\d+")))
            die_key();

        std::stringstream input_stream(input_key.data());
        input_stream.exceptions(std::ios::failbit);

        try {
            input_stream >> this->key;
        }
        catch (const std::exception& e) {
            die_key();
        }
    }

    /**
     * Output file target
     */
    void args::set_output_file(std::string_view file)
    {
        /** Catch duplicate call */
        if (!this->output_file.empty()) {
            std::cerr << "Error: Target FILE output already exists\n";
            util::exit_fail();
        }

        this->output_file = file;

        if (this->output_file.find("/") != std::string::npos) {
            std::string parent_dir = output_file.substr(0, output_file.find_last_of("/") + 1);
            if (!std::filesystem::is_directory(parent_dir)) {
                std::cerr << "Error: Invalid parent directory of output FILE\n";
                util::exit_fail();
            }
        }

    }

    /**
     * Private methods
     */

    void args::is_content_empty()
    {
        if (!this->content.empty()) {
            std::cerr << "Error: Target already exists\n";
            util::exit_fail();
        }
    }

}
