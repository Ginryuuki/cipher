#include "util.hpp"

#include <cstdlib>

namespace util {
    void exit_fail()
    {
        std::exit(EXIT_FAILURE);
    }
}
