#ifndef __CIPHER_HPP__
#define __CIPHER_HPP__

#include <string>
#include <string_view>
#include <unordered_map>
#include <memory>
#include <cstdlib>

namespace cipher {
    /**
     * Properties that the program will use
     */
    class args {
        public:
            /** Constructor */
            args(int argc, char **argv);
            
            /**
             * Public methods as getters and setters
             */
            void set_content(std::string_view content);
            void set_content_from_file(std::string_view file);
            void turn_on_decrypt();
            void set_key(std::string_view input_key);
            void set_output_file(std::string_view file);

            std::string_view get_content() { return this->content; }
            std::string_view get_output_file() { return this->output_file; }
            bool is_decrypt() { return this->decrypt; }
            int get_key() { return this->key; }
        private:
            /**
             * Variables
             */
            std::string content;
            std::string output_file;
            bool decrypt;
            int key;

            void is_content_empty();
    };

    class caesar {
        public:
            /** Constructor */
            caesar(std::unique_ptr<args> args);
        private:
            /**
             * Variables
             */
            std::string content;
            std::string processed_content;
            std::string str_map;
            std::unordered_map<char, int> char_map;
            int key;
    };
}

#endif
