#include <cstdlib>
#include <memory>

#include "cipher.hpp"

int main(int argc, char **argv)
{
    std::unique_ptr<cipher::args> args = 
        std::make_unique<cipher::args>(argc, argv);

    cipher::caesar ccipher = cipher::caesar(std::move(args));

    return EXIT_SUCCESS;
}
